# Description de mon projet Jeu Js
Dans le cadre de ce projet, j'ai réalisé un jeu en TypeScript intitulé Little Bird.Les wiframes et maquettes ont été réalisé à l'aide de [Figma](https://www.figma.com/file/UYo4T1yuZhl9eNytSEFVQD/Projet-Jeu-TypeScript?node-id=0%3A1&t=TxwsBJkLXbp6LjuD-1) 

Le projet se compose de deux parties: l'accueil et l'espace de jeu.

La structure de base est énoncée dans un fichier HTML et la mise à jour de contenu dynamique se fait via le script grâce à un fichier TypeScript lié.
Le style est pris en charge par un fichier CSS lié et il a été optimisé à l'aide de bootstraps et des media queries.

Plusieurs éléments dans le projet sont animés à l'aide d'images sprite mise en mouvement. Pour optimser cela, j'ai créé une interface ElementAnime dans un fichier Entities qui a été importée dans le script pour créer des objets pour chaque élément avec une taille globale de sprite, une taille pour chaque image du sprite et une vitesse. 
Ces objets ont ensuite pu être appelés dans une fonction startAnimation avec un deuxième paramètre, celui de l'élément HTML dans lequel s'exécute l'animation.
Cette fonction a ainsi été utilisée pour les personnages, les ennemis, les récompenses et les backgrounds du jeu. On a fait une deuxième fonction pour les backgrounds afin d'y inclure également le lancement du compteur de points.

La page d'accueil permet de présenter l'univers du jeu, ses consignes et permet de proposer 4 décors au joueur à l'aide de boutons. Des AddEventListener permettent donc de basculer à l'aide d'un ClassList.add("hide") et ClassList.remove("hide") de la partie accueil à l'espace de jeu, avec un changement au niveau des décors et des ennemis (pour l'instant seulement dans la partie nuit mais par la suite l'idée serait d'avoir des ennemis différents dans les 4 univers).

L'espace gameplay présente le personnage sur le décor choisi et lorsque l'on appuie sur une touche, le décor et le compteur se lancent. Cela va également déclencher les fonctions de collisions avec les ennemis et les récompenses et leur arrivée sur l'espace de jeu à une hauteur aléatoire.

Pour pouvoir déplacer le personnage du jeu, j'ai utilisé une fonction move qui déplace sa div grâce au systeme de keycode avec un onkeydown sur le document.

Pour le jeu, deux types de fonctions collisions ont été créées, une pour les ennemis et une pour les récompenses car une collision avec un ennemi entraine le game-over alors que chaque collision avec une récompense rapporte +20points.

Enfin une fonction gameOver a été créée en cas de collision avec un ennemi, pour arrêter le jeu, faire apparaitre le "game over"  avec le score du joueur et lui proposer de rejouer à l'aide d'un bouton "restart".



## Aperçus des wireframes et maquettes
Wireframes et Maquettes sur Figma
![<alt>](</public/img/Figma.png>)

Maquettes desktop avec les différents décors
![<alt>](</public/img/maquettesLittleBird.png>)



## Consignes du projet

Durée du projet : 2 semaines
L'objectif de ce projet est de faire un petit jeu en Javascript sans framework.
Il va permettre d'utiliser le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique... mais professionnel.
Le type et le thème du jeu sont libres (en sachant qu'il sera sur votre portfolio, donc pas n'importe quoi non plus).

Organisation:
- Choisir le thème et le type du jeu
- Faire 2-3 maquettes fonctionnelles de votre jeu
- Créer un board dans le dépôt gitlab du jeu dans lequel vous listerez toutes les fonctionnalités à coder
- Créer une milestone (un Sprint) par semaine et y indiquer l'état dans lequel vous souhaitez que le jeu soit au bout de ce sprint, assigner les fonctionnalités du board à la milestone
- Pour chaque fonction/méthode, je fais la JSDoc


Aspects techniques obligatoires:
Utilisation des objets Javascript d'une manière ou d'une autre (genre une ou deux interfaces, ça serait bien)
Avoir des données dans le JS qui seront modifiées en cours de jeu (barre de vie, score, progression, etc.), l'idée est de travailler la séparation des données et de l'affichage


Notes:
Les jeux en temps réels sont plus compliqués à gérer (genre un truc de plateforme, ou n'importe quel jeu où il se passe des choses sans action utilisateur·ice)
Les frameworks de jeu type phaserJS, bien qu'intéressant, ne sont pas autorisés car compétences trop spécifiques
Le jeu ne doit pas nécessairement être responsive (et c'est bien le seul projet qui ne le sera pas)



## Les liens concernant le projet Portfolio

- [ ] [Figma](https://www.figma.com/file/UYo4T1yuZhl9eNytSEFVQD/Projet-Jeu-TypeScript?node-id=0%3A1&t=TxwsBJkLXbp6LjuD-1) 
- [ ] [Gitlab](https://gitlab.com/Laure_G/projet-jeu)
- [ ] [Netlify](https://littlebirdgame.netlify.app/)

## Les outils utilisés

- HTML pour la structure, 
- CSS pour le style, 
- TypeScript pour l'interactivité et la mise à jour de contenu dynamique, 
- Bootstrap et les Media Queries pour le responsive, 
- Google Fonts Apis pour la police d'écriture, 
- Figma pour réaliser les wireframes et maquettes, 
- Visual Studio Code 
- Gitlab.

## Test et Mise en ligne

Le HML/ CSS de ce projet a été testé avec W3C puis a été mis en ligne en utilisant Netlify.


## Statut du projet

Ce projet pourra être amené à évoluer dans le temps, au fur et à mesure de mon parcours et de mon apprentissage.
On peut déjà noter une volonté d'optimisation des différents univers avec l'ajout d'ennemis différents (pas seulement dans le décor nuit). Et j'aimerai égalment réflechir à la version mobile, peut-être avec des boutons correspodants aux touches du claviers à utiliser pour les déplacements (les mediaqueries pour la présentation mobile de l'acceuil on déjà été fait).
