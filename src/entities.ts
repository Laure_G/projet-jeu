export interface ElementAnime {
    widthOfEachSprite: number;
    widthOfSpriteSheet: number;
    speed: number;
}