
import { ElementAnime } from "./entities";

//Les variables 
const bird = document.querySelector<HTMLDivElement>(".bird")
let horizontal:number = 0;
let vertical:number = 0;
let animationInterval: number;
let isCloud = false;
let isGameOver = false;
let isNight = false;
let score = document.querySelector <HTMLElement>("#score")
let interval = null;
let playerScore = 0;
const home = document.querySelector<HTMLElement>(".home");
const backgroundImage = document.querySelector<HTMLElement>("#background-image");
const desert = document.querySelector<HTMLButtonElement>(".desert");
const montain = document.querySelector<HTMLButtonElement>(".montain");
const cloud = document.querySelector<HTMLButtonElement>(".cloud");
const start = document.querySelector<HTMLButtonElement>(".start");
const haut = document.querySelector<HTMLButtonElement>(".haut");
const bas = document.querySelector<HTMLButtonElement>(".bas");
const gauche = document.querySelector<HTMLButtonElement>(".gauche");
const droite = document.querySelector<HTMLButtonElement>(".droite");
const night = document.querySelector<HTMLButtonElement>(".night");
const enemy = document.querySelector<HTMLElement>('.enemy-image');
const enemies = document.querySelectorAll<HTMLElement>('.enemy-image');
const gameSpace = document.querySelector<HTMLElement>('.gameSpace');
const over = document.querySelector<HTMLElement>('.over');
const recompense = document.querySelector<HTMLDivElement>(".pieces")
const recompenses = document.querySelectorAll<HTMLElement>(".pieces");
const endScore = document.querySelector<HTMLDivElement>(".endScore")
const restart = document.querySelector<HTMLButtonElement>(".restart");
const birdHome = document.querySelector<HTMLButtonElement>(".birdHome");
const pieceInHome = document.querySelector<HTMLButtonElement>(".piecesHome");
const cloudHome = document.querySelector<HTMLButtonElement>(".cloudHome");

//Les objets liés à l'animation des sprites des éléments
const birdPerso: ElementAnime = {
  widthOfEachSprite: 140,
  widthOfSpriteSheet: 560,
  speed: 80,
}
const enemyPerso: ElementAnime = {
  widthOfEachSprite: 140,
  widthOfSpriteSheet: 560,
  speed: 100,
}
const enemyBat: ElementAnime = {
  widthOfEachSprite: 222,
  widthOfSpriteSheet: 891,
  speed: 150,
}
const pieces: ElementAnime = {
  widthOfEachSprite: 44.1,
  widthOfSpriteSheet: 441,
  speed: 30,
}
const backgroundcloud: ElementAnime = {
  widthOfEachSprite: 10,
  widthOfSpriteSheet: 1000,
  speed: 30,
}
const backgroundLong: ElementAnime = {
  widthOfEachSprite: 10,
  widthOfSpriteSheet: 1920,
  speed: 30,
}

/**Fonction pour lancer l'animation des sprites des éléments du jeu
 * 
 * @param perso correspond à l'objet de type ElementAnime que l'on souhaite animer dans une div
 * @param el correspond à la div dans laquelle on souhait animé un élément
 */
function startAnimation(perso:ElementAnime, el:HTMLElement) { 
  let position = perso.widthOfEachSprite; 
  const speed: number = perso.speed; //vitesse en ms
  const diff = perso.widthOfEachSprite; //espace entre deux images
  
  animationInterval = setInterval(() => {
    if(el)
    el.style.backgroundPosition = `-${position}px 0px`;
    
    if (position < perso.widthOfSpriteSheet) {
      position = position + diff;
    } else {
      position = perso.widthOfEachSprite;
    }
  }, speed);
}

/**Fonction pour lancer l'animation des sprites des backgrounds du jeu et déclencher le compteur de point du score
 * 
 * @param back correspond au background de type ElementAnime que l'on souhaite animer dans une div
 * @param el correspond à la div dans laquelle on souhait animé un élément
 */
function startAnimationbackground(back:ElementAnime,el : HTMLElement) {
  startAnimation(back, el);
  interval = setInterval(scoreCounter, 400);
}

//Lancement de l'animation des éléments décoratifs de la home
if (birdHome && pieceInHome){
  startAnimation(birdPerso,birdHome);
  startAnimation(pieces, pieceInHome);
}

//Lancement de l'animation du décor de la Home
if (cloudHome){
  startAnimation(backgroundcloud, cloudHome );
}

//AddEventListener du bouton pour le décor desert
desert?.addEventListener('click', () => {
  if (desert && gameSpace && home && backgroundImage){
    backgroundImage.classList.add ("decorDesert");
    gameSpace.classList.remove("hide");
    home.classList.add("hide");
  }
  if (bird && enemy && recompense){
    startAnimation(birdPerso, bird);
    for (const enemy of enemies){
      startAnimation(enemyPerso, enemy);}
      for (const recompense of recompenses){
        startAnimation(pieces,recompense);
      }
    }
    setInterval(createEnemy, (randomIntFromInterval(1,24)*250));
  }
  )
  function randomIntFromInterval(min:number, max:number) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
}

//AddEventListener du bouton pour le décor montagnes
montain?.addEventListener('click', () => {
if (montain && gameSpace && home && backgroundImage){
  backgroundImage.classList.add ("decorMontain");
  gameSpace.classList.remove("hide");
  home.classList.add("hide");
}
if (bird && enemy && recompense){
  startAnimation(birdPerso, bird);
  for (const enemy of enemies){
    startAnimation(enemyPerso, enemy);}
    for (const recompense of recompenses){
      startAnimation(pieces,recompense);
    }
  }
  setInterval(createEnemy, (randomIntFromInterval(1,24)*250))
})
    
//AddEventListener du bouton pour le décor dans les nuages
cloud?.addEventListener('click', () => {
isCloud = true;
if (cloud && gameSpace && home && backgroundImage){
  backgroundImage.classList.add ("decorCloud");
  gameSpace.classList.remove("hide");
  home.classList.add("hide");
}
if (bird && enemy && recompense){
  startAnimation(birdPerso, bird);
  for (const enemy of enemies){
    startAnimation(enemyPerso, enemy);}
    for (const recompense of recompenses){
      startAnimation(pieces,recompense);
    }
  }
  setInterval(createEnemy, (randomIntFromInterval(1,24)*250))
})
      
//AddEventListener du bouton pour le décor nuit
  night?.addEventListener('click', () => {
  isNight = true;
  if (night && gameSpace && home && backgroundImage && enemy){
    backgroundImage.classList.add ("decorNight");
    for (const enemy of enemies){
      enemy.classList.add("enemyNight");
      enemy.style.width ="222px";}
      gameSpace.classList.remove("hide");
      home.classList.add("hide");
    }
    if (bird && enemy && recompense){
      startAnimation(birdPerso, bird);
      for (const enemy of enemies){
        startAnimation(enemyBat, enemy);}
        for (const recompense of recompenses){
          startAnimation(pieces,recompense);
        }    
      }
      setInterval(createEnemy,(randomIntFromInterval(1,42)*250))
})
          
/**Fonction pour lancer l'animation générale et les fonctions nécessaires au gameplay dans l'espace de jeu */
function startAnimationgame() {
  if (isCloud == true){
    if (backgroundImage){
      startAnimationbackground(backgroundcloud, backgroundImage);
    }
  }
  else {
    if (backgroundImage){
      startAnimationbackground(backgroundLong, backgroundImage);
    }
  }
  setInterval(collisionennemy, 10);
  setInterval(collisionpieces, 10);
  setInterval(enemyRandomStart,5000);
  setInterval(recompenseRandomStart,6000);
};
          
/**Fonction pour controler les déplacements du perso du jeu*/
function move(event:any) {
  if (event.keyCode == 39 && horizontal < 680) {
    horizontal += 20;
    if(bird){
      bird.style.left = Number(horizontal)+ horizontal+ "px";
    }
  }
  if (event.keyCode == 37 && horizontal > 0) {
    horizontal -= 20;
    if(bird){
      bird.style.left = Number(horizontal)+ horizontal+ "px";
    }
  }
  if(event.keyCode == 38 && vertical > -181 ) {
    vertical -= 20;
    if(bird){
      bird.style.top = Number(vertical)+ vertical+ "px";
    }
  }
  if (event.keyCode == 40 && vertical <180) {
    vertical += 20;
    if(bird){
      bird.style.top = Number(vertical)+ vertical+ "px";
    }
  }
}

document.onkeydown = move;      

//AddEventistener Start pour version mobile
start?.addEventListener('click', () => {
  startAnimationgame();
  start.style.display="none";
bas?.addEventListener('click', () => {
  if (vertical <180) {
    vertical += 20;
    if(bird){
      bird.style.top = Number(vertical)+ vertical+ "px";
    }
  }
})
haut?.addEventListener('click', () => {
  if(vertical > -220 ) {
    vertical -= 20;
    if(bird){
      bird.style.top = Number(vertical)+ vertical+ "px";
    }
  }
 
})
gauche?.addEventListener('click', () => {
  if (horizontal > -25) {
    horizontal -= 20;
    if(bird){
      bird.style.left = Number(horizontal)+ horizontal+ "px";
    }
} 
})
droite?.addEventListener('click', () => {
  if ( horizontal < 390) {
    horizontal += 20;
    if(bird){
      bird.style.left = Number(horizontal)+ horizontal+ "px";
    }
  } 
})
}); 
          
//AddEventListener pour lancer la fonction démarrage du jeu lorsque l'on appuie sur une touche
document.addEventListener("keydown",startAnimationgame, {once:true})    
     
          
/**Fonction de gestion du score*/
function scoreCounter (){
  if(isGameOver==false){
    playerScore++;
    if(score){
      score.innerHTML = "Score " + playerScore;}
      if(score && playerScore < 10){ 
        score.textContent = "Score 0" + playerScore; 
      }
    }
}
            
            
/**Fonction démarrage aléatoire sur la hauteur des ennemis*/
function enemyRandomStart(){
  for (const enemy of enemies){
    if (enemy && gameSpace){
      enemy.style.right = "0px";
      enemy.style.top = Math.random()*95+'vh';
      enemy.classList.add ("movinAnim");
    }  
  }
}
            
/**Fonction démarrage aléatoire des récompenses sur la hauteur*/
function recompenseRandomStart(){
  for (const recompense of recompenses){
    if (gameSpace && recompense){
      if (isGameOver == false){
        recompense.style.display = "block";
      }
      recompense.style.right = "0px";
      recompense.style.top = Math.random()*95+'vh';
      recompense.classList.add ("movinAnim");
      recompense.style.animationDuration ="6s";
    }  
  }
}           
            
/**Function de la collision pour tous les ennemis*/
function collisionennemy(){
  for (const enemy of enemies){
    collisionBlocEnemy(enemy);}
    const newEnemies = document.querySelectorAll<HTMLElement>('.newEnemy');
    for (const enemy of newEnemies){
      collisionBlocEnemy(enemy);}
} 
                
/**Function de la collision entre une div perso et une div ennemi*/
function collisionBlocEnemy(enemy: HTMLElement) {
  let birdCollider;
  let enemyCollider;
  if (bird && enemy) {
    birdCollider = { x: bird?.offsetLeft - bird?.scrollLeft, y: bird?.offsetTop - bird?.scrollTop, width: 60, height: 50 };
    enemyCollider = { x: enemy?.offsetLeft - enemy?.scrollLeft, y: enemy?.offsetTop - enemy?.scrollTop, width: 60, height: 50 };
    if (birdCollider.x < enemyCollider.x + enemyCollider.width &&
      birdCollider.x + birdCollider.width > enemyCollider.x &&
      birdCollider.y < enemyCollider.y + enemyCollider.height &&
      birdCollider.y + birdCollider.height > enemyCollider.y) {
        gameOver();
      }
    }
}                 
                  
/**Function en cas de collision avec une pièce bonus*/
function collisionpieces(){
let birdCollider;
let recompenseCollider;

for (const recompense of recompenses){
  if (bird && recompense  ){
    birdCollider= {x: bird?.offsetLeft - bird?.scrollLeft, y: bird?.offsetTop - bird?.scrollTop, width: 120, height: 80};
    recompenseCollider= {x: recompense?.offsetLeft - recompense?.scrollLeft, y: recompense?.offsetTop - recompense?.scrollTop, width: 42, height: 45};    
    if(birdCollider.x < recompenseCollider.x + recompenseCollider.width &&
      birdCollider.x + birdCollider.width > recompenseCollider.x &&
      birdCollider.y < recompenseCollider.y + recompenseCollider.height &&
      birdCollider.y + birdCollider.height > recompenseCollider.y) 
      {recompense.style.display = "none";
      playerScore = playerScore +20;
    }
  }
}       
}  

/**Function pour créer de nouveaux ennemis*/
function createEnemy (){
  if (isGameOver==false) {
    const enemy= document.createElement('div');
    const perso = document.querySelector<HTMLElement>('.perso');
    perso?.append(enemy);
    enemy.style.top = Math.random()*90+'vh';
    enemy?.classList.add ("newEnemy");
    enemy.classList.add ("movinAnim");
    const enemies = document.querySelectorAll<HTMLElement>('.newEnemy');
    if (enemy){
      for (const enemy of enemies){
        if (isNight == true){
          enemy.classList.add("enemyNight");
      enemy.style.width ="222px";
          startAnimation(enemyBat, enemy);
        }
        else{
          startAnimation(enemyPerso,enemy);
        }
      }  
    }
  }
}  

/**Function game over*/
function gameOver(){
  isGameOver = true
  clearInterval(animationInterval);
  if(enemy && recompense && over && endScore && bird ){
    for (const recompense of recompenses){
      recompense.style.display = "none";}
    for (const enemy of enemies){
        enemy.style.display = "none";}
    const newEnemies = document.querySelectorAll<HTMLElement>('.newEnemy');
    for (const enemy of newEnemies){
      enemy.style.display = "none";}
    bird.style. top = "-10vh";
    bird.style.left = "60vh";
    over.style.display= "block";
    endScore.innerHTML = "Ton score est de " + playerScore + " points!";
  }
}
                      
//AddEventListener du bouton restart de fin
restart?.addEventListener('click', () => {
  window.location.reload();
})
                      
                      
                           
                      